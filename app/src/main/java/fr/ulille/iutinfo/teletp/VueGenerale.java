package fr.ulille.iutinfo.teletp;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

public class VueGenerale extends Fragment {

    // TODO Q1
    private String salle;
    private String poste;
    private final String DISTANCIEL = "Distanciel";
    // TODO Q2.c
    private SuiviViewModel modele;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.vue_generale, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // TODO Q1
        // TODO Q2.c
        this.modele = new ViewModelProvider(requireActivity()).get(SuiviViewModel.class);
        // TODO Q4
        Spinner spSalle = (Spinner) getActivity().findViewById(R.id.spSalle);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this.getContext(),
                R.array.list_salles, android.R.layout.list_content);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spSalle.setAdapter(adapter);

        view.findViewById(R.id.btnToListe).setOnClickListener(view1 -> {
            // TODO Q3
            TextView textUsername = getActivity().findViewById(R.id.tvLogin);
            String username = textUsername.getText().toString();
            modele.setUsername(username);
            Toast.makeText(this.getContext(), username, Toast.LENGTH_LONG).show();

            NavHostFragment.findNavController(VueGenerale.this).navigate(R.id.generale_to_liste);
        });

        // TODO Q5.b
        Spinner spPoste = (Spinner) getActivity().findViewById(R.id.spPoste);
        update();
        spSalle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                salle = ((Spinner) getActivity().findViewById(R.id.spSalle)).toString();
                update();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        spPoste.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                poste = ((Spinner) getActivity().findViewById(R.id.spPoste)).toString();
                update();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
            // TODO Q9
    }

    // TODO Q5.a
    public void update(){
        Spinner spPoste = (Spinner) getActivity().findViewById(R.id.spPoste);
        if( DISTANCIEL.equals(this.salle)) {
            spPoste.setVisibility(View.INVISIBLE);
            spPoste.setEnabled(false);
            modele.setLocalisation(DISTANCIEL);
        } else {
            spPoste.setVisibility(View.VISIBLE);
            spPoste.setEnabled(true);
            modele.setLocalisation(this.poste +" : "+ this.salle);
        }
    }
    // TODO Q9
}